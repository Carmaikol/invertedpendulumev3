package karma;

import lejos.hardware.sensor.EV3ColorSensor;
import lejos.hardware.sensor.EV3GyroSensor;
import lejos.hardware.sensor.EV3IRSensor;
import lejos.hardware.sensor.EV3UltrasonicSensor;

import java.io.IOException;
import java.util.ArrayList;


import lejos.hardware.Brick;
import lejos.hardware.BrickFinder;
import lejos.hardware.Button;
import lejos.hardware.Key;
import lejos.hardware.ev3.LocalEV3;
import lejos.hardware.lcd.Font;
import lejos.hardware.lcd.GraphicsLCD;
import lejos.hardware.lcd.LCD;
import lejos.hardware.motor.Motor;
import lejos.hardware.port.Port;
import lejos.hardware.port.SensorPort;
import lejos.robotics.RegulatedMotor;
import lejos.robotics.SampleProvider;
import lejos.robotics.filter.AbstractFilter;
import lejos.robotics.filter.PublishFilter;
import lejos.utility.Delay;

public class Main {

    static RegulatedMotor leftMotor = Motor.D;
    static RegulatedMotor rightMotor = Motor.A;
    static IRSensor sensor;
    
    public static void introMessage() {

		  GraphicsLCD g = LocalEV3.get().getGraphicsLCD();
		  g.drawString("INVERTED PENDULUM", 5, 0, 0);
		  g.setFont(Font.getSmallFont());
	
	  	// Quit GUI button:
		g.setFont(Font.getSmallFont()); // can also get specific size using Font.getFont()
		int y_quit = 100;
		int width_quit = 45;
		int height_quit = width_quit/2;
		int arc_diam = 6;
		g.drawString("QUIT", 9, y_quit+7, 0);
		g.drawLine(0, y_quit,  45, y_quit); // top line
		g.drawLine(0, y_quit,  0, y_quit+height_quit-arc_diam/2); // left line
		g.drawLine(width_quit, y_quit,  width_quit, y_quit+height_quit/2); // right line
		g.drawLine(0+arc_diam/2, y_quit+height_quit,  width_quit-10, y_quit+height_quit); // bottom line
		g.drawLine(width_quit-10, y_quit+height_quit, width_quit, y_quit+height_quit/2); // diagonal
		g.drawArc(0, y_quit+height_quit-arc_diam, arc_diam, arc_diam, 180, 90);
		
		// Enter GUI button:
		g.fillRect(width_quit+10, y_quit, height_quit, height_quit);
		g.drawString("GO", width_quit+15, y_quit+7, 0,true);
		
		Button.waitForAnyPress();
		if(Button.ESCAPE.isDown()) System.exit(0);
		g.clear();
}
  
public static void main(String[] args) throws IOException {
	
	introMessage();
	float frequency = 0.1F; // 1 sample per second //antes 1
	//EV3IRSensor irSensor = new EV3IRSensor(SensorPort.S1);
	//SampleProvider sp = new PublishFilter(irSensor.getDistanceMode(), "IR range readings", frequency);
	//float[] sample = new float[sp.sampleSize()];
	
	
	EV3GyroSensor gyroSensor = new EV3GyroSensor(SensorPort.S2);
	SampleProvider sp3 = new PublishFilter(gyroSensor.getRateMode(), "Gyro readings", frequency);
	float[] sample3 = new float[sp3.sampleSize()];
	
	while(Button.ESCAPE.isUp()) {
		//sp.fetchSample(sample, 0);
	//	LCD.clear(3);
	//	LCD.drawString("IR: " + sample[0],0,3);
		
		sp3.fetchSample(sample3, 0);
		
		//System.out.println("SAMPLE: " + sample3[0]);
		
		LCD.clear(5);
		//LCD.drawString("Gyro: " + sample3[0],0,5);
		//LCD.drawString("TOTAL: " + sample3,1,6);
		Delay.msDelay((long) (1000/frequency));
		
		
		
		
	}
	
	
	
	
	//irSensor.close();
	gyroSensor.close();
	
}
    
	
  class IRSensor extends Thread
    {
        EV3IRSensor ir = new EV3IRSensor(SensorPort.S4);
        SampleProvider sp = ir.getDistanceMode();
        public int control = 0;
        public int distance = 255;

        IRSensor()
        {

        }
        
        public void run()
        {
            while (true)
            {
                float [] sample = new float[sp.sampleSize()];
                control = ir.getRemoteCommand(0);
                sp.fetchSample(sample, 0);
                distance = (int)sample[0];
                System.out.println("Control: " + control + " Distance: " + distance);
                
            }
            
        }
    }
    
    

   

    
    
	
}
