package karma;

import lejos.hardware.Button;
import lejos.robotics.SampleProvider;

public class Main2 {
	
	//###################################################
	//					MAIN
	// ##################################################
	
	public static void main(String[] args) {
		
		
		BalanceLoop balanceLoop = new BalanceLoop();
		
		try {
			// #####################
			//        START
			// ####################
			
			//TODO ver que variables equivalen a las formulas
			//TODO Limpiar el c�digo
			//TODO Generar algoritmos correctos
			//TODO Implementar mejoras
			
			
			balanceLoop.initialize();
			Button.LEDPattern(2);
			while(Button.ESCAPE.isUp()) {
			
			balanceLoop.position();
			balanceLoop.readEncoders();
			
			balanceLoop.readGyro();
			//balanceLoop.combineSensorValues();
			balanceLoop.PID(0,balanceLoop.combineSensorValues());
			balanceLoop.errors();
			//balanceLoop.getSteer();
			balanceLoop.setMotorPower();
			//balanceLoop.wait();	
			Thread.sleep(100);
			}
			
			
			
			
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
		
		
	}
	
	

}
