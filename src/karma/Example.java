package karma;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

import lejos.hardware.Button;
import lejos.hardware.motor.Motor;
import lejos.hardware.port.SensorPort;
import lejos.hardware.sensor.EV3GyroSensor;
import lejos.hardware.sensor.EV3IRSensor;
import lejos.robotics.RegulatedMotor;
import lejos.robotics.SampleProvider;
import lejos.robotics.filter.PublishFilter;

public class Example {
	
	   
    ////////// TESTING
    // These values must be adjusted for convenience
    private final static float accel_offset = 3.7F;
    private final static float gyro_gain = 131F;
    private final static float gyro_offset = 0.0F;

    //Set point values used in PID controller
    private final static float Kp = 17F;
    private final static float Kd = 840F;
    private final static float Ki = 0.1F;

    //For angle formula using complimentary filter
    private static float angle_filtered = 0.0F;
    private final static float pi = 3.14159F;

    //Last time in Filter calculation
    static  long preTime = 0;
    //Last time in PID calculation
    static long lastTime = 0;

    //PID sum errors
    static float errSum = 0;
    //PID last error
    static float lastErr = 0;

    //For future remote control implementation.
    static float Turn_Speed = 0, Turn_Speed_K = 0;
    static float Run_Speed = 0, Run_Speed_K = 0, Run_Speed_T = 0;

    //>0 move motor forward, <0 move motor back
    private static float LOutput;
	private static float ROutput;
    
	
	////END TEST
	
	
	public static RegulatedMotor leftMotor = Motor.D;
	public static RegulatedMotor rightMotor = Motor.A;
	//static EV3GyroSensor gyroSensor;
	static SampleProvider sp;
	static SampleProvider sp2;
	static float[] sample;
	static float[] sample2;
	public static void main(String[] args) throws IOException {
		float frequency = 0.1F; // 1 sample per second
		System.out.println("HOLA");
		EV3GyroSensor gyroSensor = new EV3GyroSensor(SensorPort.S3);
		//irSensor = new EV3IRSensor(SensorPort.S4);
		System.out.println("CREO EL GRYO");
		 try {
			// SampleProvider sp;
			// float[] sample;
			 sp = gyroSensor.getAngleAndRateMode();
			//sp = new PublishFilter(gyroSensor.getRateMode(), "Gyro readings", frequency);
			System.out.println("CREO SP");
			//sp2 = new PublishFilter(gyroSensor.getRateMode(), "Gyro speed readings", frequency);
	
		System.out.println("SAMPLE");
		 sample = new float[sp.sampleSize()];
		 System.out.println(sample[0]);
		// sample2 = new float[sp2.sampleSize()]; //get rate and angle mode dan problemas
		

		 System.out.println("PASE LOS FILTROS");
		// Looping 200 times to get the real values when starting
    	    for (int i = 0; i < 200; i++) {
    	        Filter(sp,sample);
    	    }
    	    if (Math.abs(angle_filtered) < 45) // Start the robot after cleaning data
    	    {
    	        angle_filtered = 0;
    	        Filter(sp,sample);
    	        errSum = Run_Speed = Turn_Speed = 0;
    	        PID();
    	    }
    	    
        System.out.println(angle_filtered);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				
				try {
				      Thread.sleep(100000000);
				} catch (Exception e2) {}
				
				
			}
        //Start move and balance thread
        ControlLoopThread controlLoopTask = new ControlLoopThread();
        controlLoopTask.start();

	}
	
	/**
	 * PID Control
	 */
	public static void PID() {
		
		long now = System.currentTimeMillis();
        int timeChange = (int) (now - lastTime);
        lastTime = now;
        float error = angle_filtered;  // Proportion
        errSum += error * timeChange;  // Integration
        float dErr = (error - lastErr) / timeChange;  // Differentiation
        float output = Kp * error + Ki * errSum + Kd * dErr;
        lastErr = error;
        LOutput = output - Turn_Speed + Run_Speed;
        ROutput = output + Turn_Speed + Run_Speed;
        
		
	}
	
	  /**
     * Read data from accelerometer and gyroscope and apply a complimentary
     * filter
	 * @throws IOException 
     */
    private static void Filter(SampleProvider sp, float[] sample) throws IOException {
    	
        //Data from MPU6050 Accelerometer and Gyroscope
    	float[] data = readFromGyro(sp,sample);
    	//float[] data2 = readFromGyro(sp2,sample);
       // data = accel.getMotion6();
        //Calculate angle and convert from radians to degrees
        float angle_raw = data[0];
        float omega = (float) (data[0] / gyro_gain + gyro_offset);
        // Filters data to get the real value
        long now = System.currentTimeMillis();
        float dt = (float) ((now - preTime) / 1000.00);
        preTime = now;
        //Calculate error using complimentary filter 
        float K = 0.8F;
        float A = K / (K + dt);
        angle_filtered = A * (angle_filtered + omega * dt) + (1 - A) * angle_raw;
        
    }
	
	
	public static float[] readFromGyro(SampleProvider sp,float[] sample) throws IOException {
		try {
		sp.fetchSample(sample,0);
		}catch(Exception e) {
			System.out.println("EXCEPTION"+ sample[0]);
		}
		return sample;
	}
    
     //PWM Motor control 
     
    private static void PWMControl() {
    	
    	 //SET SPEED?
        //motor.motorL_PWM((short) (Math.min(4095, Math.abs(LOutput) * 4095 / 256)));
        // motor.motorR_PWM((short) (Math.min(4095, Math.abs(ROutput) * 4095 / 256)));
    	
    	new Thread(new Runnable() {
		    public void run() {
		    	rightMotor.setSpeed((int) Math.min(4095, Math.abs(ROutput) * 4095 / 256));
		        if (ROutput > 0) {
		            //motor.moveR_Forward();
		        	rightMotor.forward();
		        } else if (ROutput < 0) {
		            //motor.moveR_Back();
		        	rightMotor.backward();
		        } else {
		            //motor.stopR();
		        	rightMotor.stop();
		        }
		    }
		}).start();
		
		new Thread(new Runnable() {
		    public void run() {
		    	leftMotor.setSpeed((int) Math.min(4095, Math.abs(LOutput) * 4095 / 256));
		    	   if (LOutput > 0) {
		           	leftMotor.forward();
		           	//motor.moveL_Forward();
		           } else if (LOutput < 0) {
		               //motor.moveL_Back();
		           	leftMotor.backward();
		           } else {
		               //motor.stopL();
		           	leftMotor.stop();
		           }
		    }
		}).start();
		
   
    	
    	
    	
     
        
       
    }
    
    public static void logToFile(String text) {
    	long now = System.currentTimeMillis();
    	List<String> lines = Arrays.asList(now+" ", text);
    	Path file = Paths.get("log.txt");
    	try {
			Files.write(file, lines, Charset.forName("UTF-8"));
		} catch (IOException e) {
			e.printStackTrace();
		}
    }
    
    
    
    static class ControlLoopThread extends Thread {
    	
        @Override
        public void run() {
        	while(Button.ESCAPE.isUp())  {
        	
        		try {
                Filter(sp,sample);
                //Logger.getGlobal().log(Level.INFO, "Angle = " + angle_filtered);
                // If angle > 45 or < -45 then stop the robot
                if (Math.abs(angle_filtered) < 45) {
                    PID();
                    PWMControl();
                } else {
                	Control.rightMotor.stop();
                	Control.leftMotor.stop();
                    //motor.stopL();
                    //motor.stopR();

                    // Keep reading accelerometer and gyroscope values after falling down
                    for (int i = 0; i < 100; i++) {
                        Filter(sp,sample);
                    }

                    if (Math.abs(angle_filtered) < 45) // Empty data and restart the robot automaticlly
                    {
                        for (int i = 0; i <= 500; i++) // Reset the robot and delay 2 seconds
                        {
                            angle_filtered = 0;
                            Filter(sp,sample);
                            errSum = Run_Speed = Turn_Speed = 0;
                            PID();
                        }
                    }

                }
        		}catch(Exception e) {
        			
        		}
            }
            Control.gyroSensor.close();
        	Control.rightMotor.close();
        	Control.leftMotor.close();
        	
        }
    }
}


