package karma.sensors;

import lejos.robotics.SampleProvider;
import lejos.robotics.filter.AbstractFilter;

public class GyroSensor extends AbstractFilter {
	
	
	float[] sample;

	public GyroSensor(SampleProvider source) {
		super(source);
		//Set the sample variable
		sample = new float[sampleSize];
	}
	
	
	public float[] readData() {
		super.fetchSample(sample,0);
		return sample;
		
	}

}
