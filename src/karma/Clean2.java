package karma;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

import lejos.hardware.Button;
import lejos.hardware.motor.Motor;
import lejos.hardware.port.SensorPort;
import lejos.hardware.sensor.EV3GyroSensor;
import lejos.hardware.sensor.EV3IRSensor;
import lejos.robotics.RegulatedMotor;
import lejos.robotics.SampleProvider;

public class Clean2 {
	
	///OLD #########################################
	
		//constantes
		private final static float diametro = 42.0F; //diametro de la rueda en mm
		private final static float radio = diametro / 2000; //en metros
		private static final float PI = 3.1416F;
		private final static float gra2rad = PI / 180.0F;

		//variables globales
		private static int velocidad = 0;
		private static int aceleracion = 50;
		private static float rotacion = 0.0F;
		private static float v = 0.0F;
		private static float k = 0.0F;
		private static float dt = 0.010F; //tiempo de iteracion del bucle de control


		private static float kp_rot = 0.0F;


		//Variables internas
		private static float g = 0.0F; //lectura giroscopio
		private static float e = 0.0F; //error
		private static float e_prev = 0.0F; //error previo
		private static float e_rot = 0.0F; //error rotacion
		private static float rot_prev = 0.0F;
		private static float de_dt = 0.0F; //derivada del error (prevision)
		private static float iedt = 0.0F; //integral del error (acumulacion)
		private static float u = 0.0F; //senal de control del PID
		private static float u_rot = 0.0F; //senal de control para la rotacion del movil
		private static float offset_gyro = 0.0F; //offset inicial en gyro

		private static float xdes = 0.0F; //error con la posicion de destino

		private static float of = 0.0F; //angulo final objetivo

		//Estados
		private static int m = 0;
		private int mobs = 0;
		private int mobsback = 0;

		private static float o = 0.0F; //angulo en el sistema de referencia

		//Variables de estado
		private static float theta = 0.0F; //angulo de inclinacion del pendulo
		private static float dtheta = 0.0F; //velocidad angular de inclinacion
		private static float x = 0.0F; //posicion
		private static float dx = 0.0F; //velocidad
		
		///OLD #########################################
		
		
		public static RegulatedMotor leftMotor = Motor.D;
		public static RegulatedMotor rightMotor = Motor.A;
		static EV3GyroSensor gyroSensor;
	    static EV3IRSensor irSensor;
	    

		//Tarea de estabilidad
				//ganancias
				static float g_th = 0.0F;
				static float g_dth = 0.0F;
				static float g_x = 0.0F;
				static float g_dx = 0.0F;

				//ganancias
				static float kp;
				static float ki;
				static float kd;

				//matriz para los encoders
				static final int n_max = 7;
				static int n = 0;
				static int n_ant = 0;
				static int[] encoder = new int[n_max];
				//TODO inicializar a 0 el array
				
				
				
				
				public static void main(String[] args) throws IOException {
					
					logToFile("Empiezo ejecucion del robot");
					System.out.println("EMPIEZA.....");
					Button.LEDPattern(1);
			        Button.discardEvents();
					//inicializamos sensores
					resetMotors();
					gyroSensor = new EV3GyroSensor(SensorPort.S3);
					SampleProvider sampleProvider = gyroSensor.getAngleAndRateMode();
					float[] sample = new float[sampleProvider.sampleSize()];
					//seteamos el offset del gyro
					float offset_gyro = offset(sampleProvider,sample);

					//matriz para los encoders
					final int n_max = 7;
					int n = 0;
					int n_ant = 0;
					int[] encoder = new int[n_max];
					
					//Valores de las constantes
					kp = 0.00442F;
					ki = 0.0481F;
					kd = 0.000000190F;

					g_th = 26F;
					g_dth = 0.200F;
					g_x = 750F;
					g_dx = 24F;
					
					logToFile("Inicializo las variables");
					System.out.println("Variables.....");
				
				//Loop de control
				while(Button.ESCAPE.isUp()) { 
						//Get angle and speed
						getAngleAndSpeed( sampleProvider,  sample);
						//obtencion de la posicion y la velocidad lineal del movil
					//	encoder[n+1] = (int) (( rightMotor).getSpeed() + (leftMotor).getSpeed() + xdes);
						encoder[n] = (int) (( rightMotor).getRotationSpeed() + (leftMotor).getRotationSpeed() + xdes);
						//x = encoder[n] * radio * gra2rad;
						x = encoder[n] * radio;
						System.out.println("Encoder X " + encoder[n]);
					//	dx = (float) ((encoder[n+1] - encoder [n_ant]) / (dt * (n_max - 1.0)) * radio * gra2rad);
						dx = (float) ((encoder[n] - encoder [n_ant]) / (dt * (n_max - 1.0)) * radio);
						System.out.println("Encoder DX " + encoder[n]);
						
						//controlador PID
						PIDControl();
						
						
						new Thread(new Runnable() {
						    public void run() {
						    	rightMotor.setSpeed((int) v*100);
						    	rightMotor.rotateTo((int) (u + u_rot),false);
						    	//rightMotor.rotateTo((int) (u - u_rot)*-1,false);
						    }
						}).start();
						
						new Thread(new Runnable() {
						    public void run() {
						    	leftMotor.setSpeed((int) v*100);
						    	leftMotor.rotateTo((int) (u - u_rot));
						    	//leftMotor.rotateTo((int) (u + u_rot)*-1);
						    }
						}).start();
						
						

						//controlador de rotacion
						if ((m < 100))
						{
							e_rot = of - o;

							//para buscar el menor sentido de giro
							if ((e_rot > PI))
							{
								e_rot = e_rot - (2 * PI);
							}
							else if (e_rot < - PI)
							{
								e_rot = e_rot + (2 * PI);
							}
							//Constante Kp del controlador P
							kp_rot = 6.0F;
							if ((Math.abs(e_rot) > gra2rad * 5))
							{
								kp_rot = 0.4F;
							}
							if (m == 10 || m == 20 || m == 30)
							{
								kp_rot = 0.20F;
							}

							rotacion = e_rot * kp_rot;
						}

						k++;
						
						n_ant = n;
						n++;
						if(n==n_max) {
							n=0;
						}

						//Condiciones l�mite
						if ((Math.abs(theta) > 60) || (Math.abs(u) > 2000))
						{
							//stopAllTasks();
							break;
						}

					}
				}

				
				
				public static float[] readFromGyro(SampleProvider sp,float[] sample) throws IOException {
					try {
					sp.fetchSample(sample,0);
					}catch(Exception e) {
						System.out.println("EXCEPTION"+ sample[0]);
					}
					return sample;
				}
				

				private static float offset(SampleProvider sp,float[] sample) throws IOException
				{
					System.out.println("OFFSET.....");
					float gyro_suma = 0.0F;
					int i;
					float gyro_valor = 0F;
					float offset_gyro = 0.0F;

					//detenemos motores
					leftMotor.setSpeed(0);
				    rightMotor.setSpeed(0);
					//sleep(500);

				    
				    for (i = 0; i < 40; i++)
					{
				    	float[] dataFromGyro = readFromGyro(sp,sample);
						gyro_valor = dataFromGyro[0];
						gyro_suma += gyro_valor;
						try {
						      Thread.sleep(50);
						} catch (Exception e2) {}
					}
					offset_gyro = gyro_suma / 40;
					System.out.println("FINOFFSET.....");
					//wait1Msec(1000);
					return (offset_gyro);
				}
				
				
				public static void getAngleAndSpeed(SampleProvider sampleProvider, float[] sample) {
					//lectura del giroscopio
					sampleProvider.fetchSample(sample, 0);
					g = sample[0];

					//obtencion de la velocidad angular y angulo de inclinacion
					dtheta = (float) (g - offset_gyro);
					offset_gyro = (float) (offset_gyro * 0.999 + (0.001 * (dtheta + offset_gyro))); //actualizamos el valor del offset
					theta = (float) (theta + dtheta * dt);
					theta = (float) (theta * 0.999 - theta * 0.001);
				//	System.out.println("Theta " + theta);

					velocidad= (int) sample[1];
					//introducimos modelo de movimiento uniforme
					//TODO caso en que sea igual???
					if ((v < velocidad * 10.0))
					{
						v = (float) (v + aceleracion * 10.0 * dt);
					}
					else if (v > velocidad * 10.0)
					{
						v = (float) (v - aceleracion * 10.0 * dt);
					}
					xdes = (int) (xdes + v * dt);

					
				}
				
				
				public static void PIDControl() {
					if (velocidad == 0)
					{
						g_dx = 24F;
						g_x = 700F;
					}
					else
					{
						g_dx = 62F;
						g_x = 750F;
					}


					e = g_th * theta + g_dth * dtheta + g_x * x + g_dx * dx;
					de_dt = (float) ((e - e_prev) / dt);
					iedt = (float) (iedt + e * dt);
					e_prev = e;
					u = (kp * e + ki * iedt + kd * de_dt) / radio;
					u_rot = rotacion / (radio);
					rot_prev = rotacion;

				}
				
			    
				public static void resetMotors() {
					leftMotor.resetTachoCount();
				    rightMotor.resetTachoCount();
				    leftMotor.rotateTo(0);
				    rightMotor.rotateTo(0);
				}
				
			    public static void logToFile(String text) {
			    	long now = System.currentTimeMillis();
			    	List<String> lines = Arrays.asList(now+" ", text);
			    	Path file = Paths.get("log.txt");
			    	try {
						Files.write(file, lines, Charset.forName("UTF-8"));
					} catch (IOException e) {
						e.printStackTrace();
					}
			    }

}
