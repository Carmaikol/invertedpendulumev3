package karma;

import lejos.hardware.motor.UnregulatedMotor;
import lejos.hardware.port.MotorPort;
import lejos.hardware.port.SensorPort;
import lejos.hardware.sensor.EV3GyroSensor;
import lejos.robotics.EncoderMotor;
import lejos.robotics.SampleProvider;

public class Test {

	// Sample provider
	float[] sample;
	SampleProvider sp;
	static EncoderMotor rightMotor;
	static EncoderMotor leftMotor;
	// Gir�scopo
	static EV3GyroSensor gyroSensor;

	float velocidad = 0;
	float aceleracion = 50;
	float dt = 0.01F; // Tiempo de bucle de control
	float v = 0;
	float offset_gyro = 0;
	float average;

	// PID
	private float Kp = 0.6F; // Proporcional
	private float Ki = 14; // Integral
	private float Kd = 0.0005F; // Derivativa

	// ganancias
	float g_th = 26;
	float g_dth = 0.2F;
	float g_x = 750;
	float g_dx = 24;

	float dtheta = 0; // Velocidad angular
	float theta = 0; // Angulo
	float xdes = 0; // Error respecto a la posicion de referencia

	int n_max = 7;
	int n = 0;
	int n_ant = 0;

	float x = 0; // Posicion
	float dx = 0; // Velocidad
	float encoder[] = new float[n_max]; //

	float g = 0; // funcion de transferencia?
	float e = 0; // error
	float e_prev = 0; // error previo
	float e_rot = 0; // error de rotacion
	float de_dt = 0;
	float iedt = 0;

	float u = 0;
	float u_rot = 0;

	// CONSTANTES
	final int DIAMETRO = 42; // mm
	final float RADIO = DIAMETRO / 2000; // m
	final float gradrad = 3.14159F / 180F;

	public void offset() throws InterruptedException {
		float gMax = 300000;
		float gMin = -30000;
		float g;
		int gyro_suma;
		sp = gyroSensor.getAngleAndRateMode();
		sample = new float[sp.sampleSize()];
		while (Math.abs(gMax - gMin) > 2) {
			gyro_suma = 0;
			offset_gyro = 0;
			// reset motores
			Thread.sleep((long) 0.5);
			gMax = 300000;
			gMin = -30000;

			for (int i = 0; i < 40; i++) {
				g = gyroRate(); // offset (radians)
				offset_gyro = g;

			}
			offset_gyro = offset_gyro / 40;
		}

	}

	public void initialize() throws InterruptedException {
		rightMotor = new UnregulatedMotor(MotorPort.A);
		leftMotor = new UnregulatedMotor(MotorPort.D);
		gyroSensor = new EV3GyroSensor(SensorPort.S3);

		offset();

		// Poner a cero todo el array encoder

	}

	public void balance() {

		g = gyroRate();
		// Sleep(0.2)
		g = g + gyroRate();

		dtheta = g / 2.0F - offset_gyro; // Velocidad angular (grados/s)
		offset_gyro = offset_gyro * 0.999F + (0.001F * (dtheta + offset_gyro)); // update offset
		theta = theta + dtheta * dt; // Angulo
		n = n + 1;
		n = (n == n_max) ? 0 : n;
		xdes = 0;
		x = rightMotor.getTachoCount() + leftMotor.getTachoCount(); // Posicion: Rotacion del motor
		n_ant = n + 1;
		n_ant = (n_ant == n_max) ? 0 : n_ant;
		encoder[n] = x;
		average = 0;
		for (int i = 0; i < n_max; i++) {
			average = average + encoder[i];
		}
		average = average / n_max;
		dx = average / dt; // Posicion : velocidad del motor

		// PID
		float k1 = 26.5F;
		float k2 = 1.56F;
		float k3 = 0.15F;
		float k4 = 0.13F;

		e = (k1 * theta + k2 * dtheta + k3 * x + k4 * dx);
		de_dt = (e - e_prev) / dt;
		iedt = iedt + e * dt;
		e_prev = e;

		if (e > 1050) {
			e = 1050;
		} else if (e < -1050) {
			e = -1050;
		}

		// v = int(e/maxMotorSpeed*100);
		moveMotors(e);

	}

	/**
	 * Get the rate from the gyroscope sensor
	 * 
	 * @param sp
	 * @param sample
	 * @return
	 */
	float gyroRate() {
		// Read from Gyro
		try {
			sp.fetchSample(sample, 0);
		} catch (Exception e) {
			// System.out.println("EXCEPTION"+ sample[0]);
		}

		// Hace falta esta conversi�n?
		// return sample[1]/(-4);
		return sample[1];

	}

	/**
	 * Set the motor speed and rotation. robot_speed or speed or ang_vel?
	 * robot_position or ang?
	 */
	public void moveMotors(final float pwr) {

		// TODO Como escribir la potencia, o f�rmula para calcular los grados,
		// velocidad...

		new Thread(new Runnable() {
			public void run() {
				rightMotor.setPower((int) pwr);
				// rightMotor.forward();
				// rightMotor.rotateTo(steering,false);
			}
		}).start();

		new Thread(new Runnable() {
			public void run() {

				leftMotor.setPower((int) pwr);
				// rightMotor.forward();
				// leftMotor.rotateTo((int) (-robot_position));
				// leftMotor.rotateTo((int) (u + u_rot)*-1);
			}
		}).start();
	}

}
