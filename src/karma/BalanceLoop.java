package karma;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

import karma.sensors.GyroSensor;
import lejos.hardware.Button;
import lejos.hardware.motor.Motor;
import lejos.hardware.motor.UnregulatedMotor;
import lejos.hardware.port.MotorPort;
import lejos.hardware.port.SensorPort;
import lejos.hardware.sensor.EV3GyroSensor;
import lejos.robotics.EncoderMotor;
import lejos.robotics.RegulatedMotor;
import lejos.robotics.SampleProvider;
import lejos.robotics.filter.PublishFilter;

public class BalanceLoop {

	float refpos; // posici�n de referencia
	float speed; // velocidad
	float dt; // aceleracion
	private static final float PI = 3.1416F;
	private final static float gra2rad = PI / 180.0F;

	// Variables de estado
	// private static float theta = 0.0F; //angulo de inclinacion del pendulo
	// private static float dtheta = 0.0F; //velocidad angular de inclinacion
	// private static float x = 0.0F; //posicion
	// private static float dx = 0.0F; //velocidad

	// Variables internas
	// private static float g = 0.0F; //lectura giroscopio
	// private static float e = 0.0F; //error
	// private static float e_prev = 0.0F; //error previo
	// private static float e_rot = 0.0F; //error rotacion
	// private static float rot_prev = 0.0F;
	// private static float de_dt = 0.0F; //derivada del error (prevision)
	// private static float iedt = 0.0F; //integral del error (acumulacion)
	// private static float u = 0.0F; //senal de control del PID
	// private static float u_rot = 0.0F; //senal de control para la rotacion del
	// movil
	// private static float offset_gyro = 0.0F; //offset inicial en gyro
	// private static float xdes = 0.0F; //error con la posicion de destino

	// private static float of = 0.0F; //angulo final objetivo

	// gyroType??

	// PID
	private float Kp = 0.6F; // Proporcional
	private float Ki = 14; // Integral
	private float Kd = 0.0005F; // Derivativa
	// gain
	// g_th, g_dthh. g_x, g_dx
	float gain_wheel_position = 350;
	float gain_wheel_speed = 75;
	float gain_motor_speed = gain_wheel_speed;
	float gain_motor_position = gain_wheel_position;
	float gain_angle = 25;
	float gain_angular_velocity = 1.3F;

	// Robot real
	float robot_speed; // dx
	float robot_position; // x
	float sampleTime = 22; // Tiempo de muestra
	float wheel_diameter = 42F; // Diametro de la rueda (mm)
	float radius; // Radio (m)

	// matriz encoders
	float[] enc_val; // array containing the encoders
	int max_index = 7; // maximo indice del array
	int enc_index; // indice principal
	int compare_index; // indice secundario

	// Error handling
	boolean NowOutOfBound;
	boolean PrevOutOfBound;
	boolean OutOfBound;
	int OutOfBoundCount;

	// angulos
	float ang; // angulo
	float mean_ang; // angulo medio
	float mean; // media
	float ang_vel; // velocidad angular

	//
	float max_acceleration;
	float steering;

	// GyroSensor gyroSensor;
	// public UnregulatedMotor leftMotor;// = Motor.D;
	// public UnregulatedMotor rightMotor;// = Motor.A;
	static EncoderMotor rightMotor;
	static EncoderMotor leftMotor;
	// public static RegulatedMotor leftMotor = Motor.D;
	// public static RegulatedMotor rightMotor = Motor.A;
	// Gir�scopo
	static EV3GyroSensor gyroSensor;

	// errors PID
	float curr_err;
	float prev_err;
	float acc_err;
	float dif_err;

	// PWM Control
	float curr_val;
	float average_power;
	float old_steering;

	// Sample provider
	float[] sample;
	SampleProvider sp;

	// ###################################################
	// METHODS
	// ##################################################

	/**
	 * Initialize program
	 * 
	 * @throws InterruptedException
	 */
	void initialize() throws InterruptedException {
		dt = (sampleTime - 2) / 1000;
		radius = wheel_diameter / 2000; // 2???
		Button.LEDPattern(1);

		// TachoMotorPort.BACKWARD 2
		// TachoMotorPort.FLOAT 4
		// TachoMotorPort.FORWARD 1
		// TachoMotorPort.MAX_POWER 100
		// TachoMotorPort.PWM_BRAKE 1
		// TachoMotorPort.PWM_FLOAT 0
		// TachoMotorPort.STOP 3
		// rigthMotor = new UnregulatedMotor(MotorPort.A,2)
		rightMotor = new UnregulatedMotor(MotorPort.A);
		leftMotor = new UnregulatedMotor(MotorPort.D);
		gyroSensor = new EV3GyroSensor(SensorPort.S3);

		// Erase array
		enc_val = new float[max_index];
		for (int i = 0; i < max_index; i++) {
			enc_val[i] = 0;
		}

		// Reiniciar motor A
		// Reiniciar motor D
		resetMotors();

		OutOfBoundCount = 0;
		refpos = 0;

		Thread.sleep(10);

		// #################
		// CALIBRATE
		// #################
		// Se toman una serie de
		// muestras para calcular
		// la media (mean).
		// #################

		// Hacer sonido
		Thread.sleep(1);
		mean = 0;
		sp = gyroSensor.getAngleAndRateMode();
		sample = new float[sp.sampleSize()];
		for (int i = 0; i < 20; i++) {
			mean += gyroRate();
			Thread.sleep((long) 0.5);
		}
		mean /= 20;
		System.out.println("MEAN: " + mean);
		// logToFile("mean: " + mean);

		speed = 0;
		steering = 0;
		max_acceleration = 0;
	}

	/**
	 * Get the rate from the gyroscope sensor
	 * 
	 * @param sp
	 * @param sample
	 * @return
	 */
	float gyroRate() {
		// Read from Gyro
		try {
			sp.fetchSample(sample, 0);
		} catch (Exception e) {
			// System.out.println("EXCEPTION"+ sample[0]);
		}

		// Hace falta esta conversi�n?
		// return sample[1]/(-4);
		return sample[1];

	}

	/**
	 * Returns the updated reference position
	 */
	void position() {
		refpos += (speed * dt * 0.002);
		// logToFile("refpos: " + refpos);
	}

	void readEncoders() {
		// get rotacion motor A
		// int rightMotorSpeed = rightMotor.getSpeed();
		// get rotacion motor b
		// int leftMotorSpeed = leftMotor.getSpeed();
		// ESTO CREO QUE NO ES CORRECTO
		// get rotacion motor A
		int rightMotorAngle = rightMotor.getTachoCount();
		// get rotacion motor b
		int leftMotorAngle = leftMotor.getTachoCount();

		System.out.println("rightMotorAngle: " + rightMotorAngle);
		System.out.println("leftMotorAngle: " + leftMotorAngle);

		// logToFile("rightMotorAngle: " + rightMotorAngle);
		// logToFile("leftMOtorAngle: " + leftMotorAngle);
		getMotorSpeed(rightMotorAngle, leftMotorAngle);

		// Velocidad del robot
		robot_speed = (radius * speed) / 57.3F;
		// logToFile("robot_speed: " + robot_speed);
		// Posicion del robot
		float aux_grados = rightMotorAngle + leftMotorAngle / 2;
		robot_position = (radius * aux_grados) / 57.3F;
		// logToFile("robot_position: " + robot_position);

	}

	void getMotorSpeed(int rightMotorAngle, int leftMotorAngle) {
		enc_index += 1;
		if (max_index == enc_index) {
			enc_index = 0;
		}
		compare_index = enc_index + 1;
		if (max_index == compare_index) {
			compare_index = 0;
		}

		enc_val[enc_index] = rightMotorAngle + leftMotorAngle / 2;
		// Es esto correcto
		speed = (enc_val[enc_index] - enc_val[compare_index]) / (max_index * dt);// / (dt * (n_max - 1.0)) * radio *
																					// gra2rad);

	}

	void readGyro() {
		curr_val = gyroRate();
		mean = (mean * (1 - (dt * 0.2F))) + (curr_val * ang_vel);
		ang_vel = mean - curr_val;
		ang += (ang_vel * dt);

		System.out.println("ang_vel: " + ang_vel);
		System.out.println("ang: " + ang);

	}

	float combineSensorValues() {

		// TODO motor_position = robot_position?????? SI
		// TODO motor_speed = robot_speed????? SI

		float y = gain_motor_position * (robot_position - refpos);
		float w = gain_motor_speed * robot_speed;
		float x = gain_angle * ang;
		float z = gain_angular_velocity * ang_vel;

		return x + y + w + z;
	}

	void PID(float reference, float input) {
		curr_err = reference - input;
		acc_err = acc_err + curr_err * dt;
		dif_err = (curr_err - prev_err) / dt;
		float P = Kp * curr_err;
		float I = Ki * acc_err;
		float D = Kd * dif_err;

		float PID = P + I + D;
		average_power = PID;
		System.out.println("average_power: " + average_power);

	}

	void errors() {
		if (average_power > 100) {
			NowOutOfBound = true;
		}
		if (NowOutOfBound && PrevOutOfBound) {
			OutOfBoundCount++;
		} else {
			OutOfBoundCount = 0;
		}
		if (OutOfBoundCount > 20) {
			// Wat 0.1
			// apagar motores
			// sonido de error
			// interrumpir bucle
		} else {
			PrevOutOfBound = NowOutOfBound;
		}
	}

	float get_steer() {
		return steering;
	}

	void setMotorPower() {

		float extra_pwr;
		float new_steering = 0;
		float pwr_A, pwr_D;
		float synco = 0;

		// MAybe esta forma es mejor
		// u = (kp * e + ki * iedt + kd * de_dt) / radio;
		// u_rot = rotacion / (radio);
		// leftMotor.rotateTo((int) (u - u_rot));

		if (steering > 50)
			new_steering = 50;
		else if (steering < -50)
			new_steering = -50;
		else
			new_steering = steering;
		// new_steering = steering limit (-50,50);

		if (new_steering == 0) {
			// get rotacion motor A
			int rightMotorAngle = rightMotor.getTachoCount();
			// get rotacion motor b
			int leftMotorAngle = leftMotor.getTachoCount();
			if (old_steering != 0) {

				synco = rightMotorAngle - leftMotorAngle;
			}
			extra_pwr = (rightMotorAngle - leftMotorAngle - synco) * 0.05F;
		} else {
			extra_pwr = new_steering * (-0.5F);
		}

		pwr_D = average_power - extra_pwr;
		pwr_A = average_power + extra_pwr;
		old_steering = new_steering;
		System.out.println("pwr_D: " + pwr_D);
		System.out.println("pwr_A: " + pwr_A);
		System.out.println("old_steering: " + old_steering);
		// Aqu� se l�a.
		moveMotors((pwr_D * 0.021F) / radius, (pwr_A * 0.021F) / radius);

		// SetPotenciaA((pwr_A*0.021F)/radius);
		// SetPotenciaD((pwr_D*0.021F)/radius);

		// WAIT

		// TEMPORIZADOR >= dt

	}

	// ##################################### UTILS
	// ##############################################################################

	/**
	 * Set the motor speed and rotation. robot_speed or speed or ang_vel?
	 * robot_position or ang?
	 */
	public void moveMotors(final float pwr_D, final float pwr_A) {

		// TODO Como escribir la potencia, o f�rmula para calcular los grados,
		// velocidad...

		new Thread(new Runnable() {
			public void run() {
				rightMotor.setPower((int) pwr_A);
				// rightMotor.forward();
				// rightMotor.rotateTo(steering,false);
			}
		}).start();

		new Thread(new Runnable() {
			public void run() {

				leftMotor.setPower((int) pwr_D);
				// rightMotor.forward();
				// leftMotor.rotateTo((int) (-robot_position));
				// leftMotor.rotateTo((int) (u + u_rot)*-1);
			}
		}).start();
	}

	/**
	 * Reset the motors.
	 */
	public static void resetMotors() {
		leftMotor.resetTachoCount();
		rightMotor.resetTachoCount();
		// leftMotor.rotateTo(0);
		// rightMotor.rotateTo(0);
	}

	/**
	 * Method to log a text into a file
	 * 
	 * @param text
	 */
	public static void logToFile(String text) {
		long now = System.currentTimeMillis();
		List<String> lines = Arrays.asList(now + ":   ", text);
		Path file = Paths.get("log.txt");
		try {
			Files.write(file, lines, Charset.forName("UTF-8"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
