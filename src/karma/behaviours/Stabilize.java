package karma.behaviours;

import karma.Control;
import lejos.hardware.Button;
import lejos.robotics.subsumption.Behavior;

class Stabilize implements Behavior
{

  private boolean _suppressed = false;

  public boolean takeControl()
  {
      if (Button.readButtons() != 0)
      {
          _suppressed = true;
          Control.leftMotor.stop();
          Control.rightMotor.stop();          
          Button.LEDPattern(6);
          Button.discardEvents();
          System.out.println("Button pressed");
          if ((Button.waitForAnyPress() & Button.ID_ESCAPE) != 0)
          {
              Button.LEDPattern(0);
              System.exit(1);
          }
          System.out.println("Button pressed 2");
          Button.waitForAnyEvent();
          System.out.println("Button released");
      }
    return true;  // this behavior always wants control.
  }

  public void suppress()
  {
    _suppressed = true;// standard practice for suppress methods
  }

  public void action()
  {
    _suppressed = false;
    //EV3BumperCar.leftMotor.forward();
    //EV3BumperCar.rightMotor.forward();
    while (!_suppressed)
    {
      //EV3BumperCar.leftMotor.forward();
      //EV3BumperCar.rightMotor.forward();
        switch(0)
        {
        case 0:
        	Control.leftMotor.setSpeed(400);
        	Control.rightMotor.setSpeed(400);
        	Control.leftMotor.stop(true);
        	Control.rightMotor.stop(true);
            break;
        case 1:
        	Control.leftMotor.setSpeed(400);
        	Control.rightMotor.setSpeed(400);
        	Control.leftMotor.forward();
        	Control.rightMotor.forward();
            break;
        case 2:
        	Control.leftMotor.backward();
        	Control.rightMotor.backward();
            break;
        case 3:
        	Control.leftMotor.setSpeed(200);
        	Control.rightMotor.setSpeed(200);
        	Control.leftMotor.forward();
        	Control.rightMotor.backward();
            break;
        case 4:
        	Control.leftMotor.setSpeed(200);
        	Control.rightMotor.setSpeed(200);
        	Control.leftMotor.backward();
        	Control.rightMotor.forward();
            break;

            
        }
      Thread.yield(); //don't exit till suppressed
    }
    //EV3BumperCar.leftMotor.stop(true); 
    //EV3BumperCar.rightMotor.stop(true);
    //EV3BumperCar.leftMotor.
  }
}
